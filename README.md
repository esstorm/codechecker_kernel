# codechecker_kernel

`codechecker_kernel` is a kernel to run CodeChecker inside a Jupyter notebook.
Make sure you've installed CodeChecker first and that it's in your path when you launch Jupyter.
https://github.com/ericsson/codechecker

## Installation
To install `codechecker_kernel`:

``` bash
git clone https://gitlab.com/esstorm/codechecker_kernel
cd codechecker_kernel
pip3 install .
python3 -m codechecker_kernel.install
```
