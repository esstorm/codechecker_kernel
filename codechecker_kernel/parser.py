import re
import regex
import logging
import sys

logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                    level=logging.INFO, stream=sys.stdout)

from .utils import debug, Component

class Parser(Component):
    def __init__(self, code):
        self.code = code
        self.filename = ""
        self.output_directory = "/tmp"
        self.enabled_checkers = []
        self.disabled_checkers = []
        self.shouldStore = False

    def parse(self):
        """
        Parses the document and checks for special commands
        """
        tokens = self.findTokens()

        self.parseTokens(tokens)

        logging.info(self.enabled_checkers)
        logging.info(self.disabled_checkers)

        if(self.filename == ""):
            self.filename = self.output_directory + "/" + "file.cc"
        else:
            self.filename = self.output_directory + "/" + self.filename

        # Write as temp file, check if a name has been specified?
        with open(self.filename, "w") as tmpFile:
            tmpFile.write(self.code)

    def findTokens(self):
        """
        This function matches the special actions, which are passed down in the form
        of single-line C comments
        """
        cmd_re = r'\/\/\s*%cmd:\s*(.*)'
        matched = re.findall(cmd_re, self.code)

        checkers_re = r'\/\/\s*%checkers:\s*(.*)'
        self.enabled_checkers = re.findall(checkers_re, self.code)

        checkers_re = r'\/\/\s*%\!checkers:\s*(.*)'
        self.disabled_checkers = re.findall(checkers_re, self.code)

        logging.info(matched)
        return matched

    def parseTokens(self, tokens):
        logging.info(tokens)
        for token in tokens:
            cmd = token.split()
            logging.info(cmd)
            if cmd[0] == 'store':
                if len(cmd) == 2:
                    self.filename=cmd[1]
                    self.shouldStore = True

            if cmd[0] == 'z3':
                logging.info('z3 is enabled!')

    def setFilename(self, filename):
        self.filename = filename

    def getFilename(self):
        return self.filename

    def getEnabledCheckers(self):
        return self.enabled_checkers

    def getDisabledCheckers(self):
        return self.disabled_checkers

    def shouldStoreResults(self):
        return self.shouldStoreResults