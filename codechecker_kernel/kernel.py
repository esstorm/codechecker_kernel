from ipykernel.kernelbase import Kernel
from subprocess import Popen, PIPE, call, CalledProcessError
import re

from .client import CmdClient
from .parser import Parser

import logging
import sys

logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                    level=logging.INFO, stream=sys.stdout)

class CCKernel(Kernel):
    implementation = 'CodeChecker'
    implementation_version = '1.0'
    language = 'no-op'
    language_version = '0.1'
    language_info = {'name': 'c++',
                     'codemirror_mode': 'text/x-c++src',
                     'mimetype': ' text/x-c++src',
                     'file_extension': '.cc'
    }
    banner = "Analyze C/C++ code inside Jupyter notebooks using Clang SA"

    def do_execute(self, code, silent, store_history=True, user_expressions=None,
                   allow_stdin=False):
        if not silent:
            parser = Parser(code)

            parser.parse()
            filename = parser.getFilename()
            enabled_checkers = parser.getEnabledCheckers()
            disabled_checkers = parser.getDisabledCheckers()

            client = CmdClient()

            with open(filename, "w") as tmpFile:
                logging.info(filename)
                tmpFile.write(code)

            result = client.analyze(filename, enabled_checkers, disabled_checkers)

            filtered_results = self.filter_results(result.decode('UTF-8'))

            stream_content = {'name': 'stdout', 'text': filtered_results}
            self.send_response(self.iopub_socket, 'stream', stream_content)

            if parser.shouldStoreResults():
                logging.info("Storing")
                client.init()
                client.store("jupyter", "/tmp/jupyter_cc")

        return {'status': 'ok',
                # The base class increments the execution count
                'execution_count': self.execution_count,
                'payload': [],
                'user_expressions': {},
               }

    def filter_results(self, result):
        filtered_string = ""
        for line in result.splitlines():
            if line.startswith('[INFO'):
                continue
            elif line.startswith('----==== Summary ====----'):
                break
            else:
                filtered_string += line + '\n'
        logging.info(filtered_string)
        return filtered_string
