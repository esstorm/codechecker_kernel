import functools
import os
import re
import subprocess
import sys
import time

def timer(fun):
    @functools.wraps(fun)
    def wrapper(*args, **kwargs):
        start = time.perf_counter()
        result = fun(*args, **kwargs)
        end = time.perf_counter()
        duration = end - start
        print("TIMER: {fun}: {time:.4f} secs".format(
            fun=fun.__name__,
            time=duration
        ))
        return result
    return wrapper

def debug(fun):
    @functools.wraps(fun)
    def wrapper(*args, **kwargs):
        _args = list(map(repr, args))
        _kwargs = list(map(lambda k, v: "{}={}".format(k, v), kwargs.items()))
        signature = ", ".join(_args + _kwargs)
        print("DEBUG: Called {fun}({params})".format(
            fun=fun.__name__, params=signature))
        result = fun(*args, **kwargs)
        print("DEBUG: {fun} returned: {result}".format(
            fun=fun.__name__, result=result))
        return result
    return wrapper

def which(command, paths=None):
    """which(command, [paths]) - Look up the given command in the paths string
    (or the PATH environment variable, if unspecified)."""

    if paths is None:
        paths = os.environ.get('PATH', '')

    # Check for absolute match first.
    if os.path.exists(command):
        return command

    # Would be nice if Python had a lib function for this.
    if not paths:
        paths = os.defpath

    # Get suffixes to search.
    if os.pathsep == ';':
        pathext = os.environ.get('PATHEXT', '').split(';')
    else:
        pathext = ['']

    # Search the paths...
    for path in paths.split(os.pathsep):
        for ext in pathext:
            p = os.path.join(path, command + ext)
            if os.path.exists(p):
                return p

    return None

def getClangVersion(clangPath=None):
    if clangPath is not None:
        if not os.path.exists(clangPath):
            print("Error: cannot find " + clangPath)
            sys.exit(1)
    else:
        clangPath = which("clang")

    runCmd = clangPath + " --version"
    version = subprocess.getoutput("clang --version")

    pattern = '(\d+\.\d+)[^ ]*'

    return(re.search(pattern, version).group(0))


class Component:

    def __init__(self, name='Component', description=''):
        self._name = name
        self._description = description

    def parseArgs(self, parser):
        groupParser = parser.add_argument_group(
            title=self._name, description=self._description)
        return groupParser

    def init(self, *args, **kwargs):
        self._args = args
        self._kwargs = kwargs

    def clean(self):
        pass      