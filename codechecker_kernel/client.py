#!/usr/bin/env python3
"""
The CodeChecker client communicator allows for interacting with a running CodeChecker
server instance.
It is used for dumping the raw bug report details from stored runs, store additional
results and to deleting runs. The interactions are with the CodeChecker command line
interface, for each query a subprocess is open from withing this Python program.
"""

### ================================== STANDARDS =================================== ###
import re
from subprocess import Popen, PIPE, call, CalledProcessError, check_output
import argparse
import sys
import json

import logging
import sys

logging.basicConfig(format='%(asctime)s | %(levelname)s : %(message)s',
                    level=logging.INFO, stream=sys.stdout)

### ================================== USER DEFINED ================================ ###
from .utils import debug, Component

### ================================== CLASSES ===================================== ###
class Client(Component):
    """
    Base class for ClientCmd.
    Establishes the connection between this client module and the CodeChecker server
    """
    def communicator(self, command):
        """
        CodeChecker CLI wrapper
        """
        runCmd = 'CodeChecker {}'.format(command)

        print(runCmd)
        proc = Popen([runCmd],
                     stdout=PIPE,
                     shell=True)

        queryRes, _ = proc.communicate()

        return (queryRes, proc.returncode)

class CmdClient(Client):
    """
    Inherits from Client and implements the interactions with CodeChecker.
    """

    def __init__(self):
        """
        Default constructor
        """
        super().__init__(name='Command line client',
                         description='Connects to the codechecker database through the command line interface')

    def parseArgs(self, parser):
        """
        Arguments parser
        """
        parser = super().parseArgs(parser)
        parser.add_argument('--host', type=str, dest='host')
        parser.add_argument('--port', '-p', type=int, dest='port')
        parser.add_argument('--product', type=str, dest='product')
        parser.add_argument('--run', '-r', type=str, dest='run', default='*')
        return ['host', 'port', 'product', 'run']

    def init(self, host='localhost', port=8001, product='Default', run='*'):
        """
        Initialize with args from argParser
        """
        self._serverHost = host
        self._serverPort = port
        self._product = product
        self._run = run

    def analyze(self, filename, enabled_checkers=[], disabled_checkers=[]):
        """
        Run the analyzer on this file
        """
        print(filename)
        runCmd = """check --print-steps -b 'clang -c {}' -c -o /tmp/jupyter_cc""".format(filename)

        if enabled_checkers:
            enabled_checkers = enabled_checkers[0].split()
            for checker in enabled_checkers:
                runCmd += ' -e {}'.format(checker)

        if disabled_checkers:
            disabled_checkers = disabled_checkers[0].split()
            for checker in disabled_checkers:
                runCmd += ' -d {}'.format(checker)

        logging.info("********* Command: " + runCmd)
        
        queryRes, returncode = self.communicator(runCmd)
        print ("***")
        # results = json.loads(queryRes) if queryRes != b'' else ''

        return queryRes

    def healthCheck(self):
        """
        Performs healthcheck on the connectivity to the CodeChecker server.
        :return ConnectionSuccessful: if the server is reachable
        """
        runCmd = 'cmd runs --url {host}:{port}/{product} -o plaintext'\
            .format(host=self._serverHost,
                    port=self._serverPort,
                    product=self._product)
        try:
            output = check_output([runCmd],
                            shell=True)
            returncode = 0
        except CalledProcessError as e:
            returncode = e.returncode

        return returncode

    def query(self, run='None'):
        """
        Query the CodeChecker server by run name. Regular expressions are supported
        returns json object with bug reports per selected runs
        """

        if run == 'None':
            run = self._run

        cmd = 'cmd results --details --url {host}:{port}/{product} "{run}" --review-status -o json'\
            .format(host=self._serverHost,
                    port=self._serverPort,
                    product=self._product,
                    run=self._run)

        queryRes, _ = self.communicator(cmd)

        results = json.loads(queryRes) if queryRes != b'' else ''

        return [results]

    def store(self, projectName, dir):
        """
        Stores the results from an analysis in the running server
        Parameters:
            projectName: name used for storing the run
            dir: path to the analysis results

        Returns    
            True: if storing was successful
            False: otherwise
        """
        cmd = 'store -f --url {host}:{port}/{product} -n {projectName} {dir}'\
            .format(host=self._serverHost,
                    port=self._serverPort,
                    product=self._product,
                    projectName=projectName,
                    dir=dir)

        logging.info(cmd)
                    
        queryRes, returncode = self.communicator(cmd)
        logging.info(queryRes.decode('UTF-8'))
        return returncode

    def createProduct(self, product):
        """
        Create a product in the CodeChecker server
        """
        cmd = 'cmd products add --url {host}:{port} {product}'\
            .format(host=self._serverHost,
                    port=self._serverPort,
                    product=product)

        queryRes, returncode = self.communicator(cmd)

        return returncode


if __name__ == "__main__":
    client = CmdClient()
    parser = argparse.ArgumentParser(description='Codechecker client')
    client.parseArgs(parser)
    client.init()
    if not client.healthCheck() == 0:
            print(
                "Please configure the correct host and port for connecting to CodeChecker!")
            sys.exit()
    client.query()
    client.clean()
